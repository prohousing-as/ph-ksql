// Copyright Prohousing AS
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package phksql

import (
	"encoding/json"
	"fmt"
	"reflect"
)

type Row []interface{}

func Unmarshal(source []byte, target interface{}) error {
	tagName := "ksql"
	ksqlRow, err := decodeJson(source)
	if err != nil {
		return err
	}
	t := reflect.TypeOf(target).Elem() // elem to dereference pointer
	v := reflect.ValueOf(target).Elem()
	if v.Kind() == reflect.Ptr {
		return fmt.Errorf("Can not unmarshal to pointer")
	}

	for i := 0; i < t.NumField(); i++ {
		if v.Field(i).CanSet() {
			switch t.Field(i).Type.Kind() {
			case reflect.Bool:
				tag := t.Field(i).Tag.Get(tagName)
				if vSet, ok := ksqlRow[tag].(bool); ok {
					v.Field(i).SetBool(vSet)
				}
			case reflect.Int:
				tag := t.Field(i).Tag.Get(tagName)
				if vSet, ok := ksqlRow[tag].(int); ok {
					v.Field(i).SetInt(int64(vSet))
				}
			case reflect.Int32:
				tag := t.Field(i).Tag.Get(tagName)
				if vSet, ok := ksqlRow[tag].(int32); ok {
					v.Field(i).SetInt(int64(vSet))
				}
			case reflect.Int64:
				tag := t.Field(i).Tag.Get(tagName)
				if vSet, ok := ksqlRow[tag].(int64); ok {
					v.Field(i).SetInt(vSet)
				}
			case reflect.String:
				tag := t.Field(i).Tag.Get(tagName)
				if vSet, ok := ksqlRow[tag].(string); ok {
					v.Field(i).SetString(vSet)
				}
			case reflect.Float32:
				tag := t.Field(i).Tag.Get(tagName)
				if vSet, ok := ksqlRow[tag].(float32); ok {
					v.Field(i).SetFloat(float64(vSet))
				}
			case reflect.Float64:
				tag := t.Field(i).Tag.Get(tagName)
				if vSet, ok := ksqlRow[tag].(float64); ok {
					v.Field(i).SetFloat(vSet)
				}
			}
		}
	}
	return nil
}

func decodeJson(source []byte) (map[string]interface{}, error) {
	var interfaceSource []interface{}
	err := json.Unmarshal(source, &interfaceSource)
	if err != nil {
		return nil, err
	}
	if len(interfaceSource) == 0 {
		return nil, fmt.Errorf("no header in row")
	}
	if len(interfaceSource) == 1 {
		return nil, fmt.Errorf("only returned header")
	}
	ksqlColNames := []string{}
	ksqlValues := Row{}
	for _, value := range interfaceSource {
		switch obj := value.(type) {
		case map[string]interface{}:
			if kt, ok := obj["columnNames"].([]interface{}); ok {
				for _, v := range kt {
					ksqlColNames = append(ksqlColNames, v.(string))
				}
			} else {
				return nil, err
			}
		case []interface{}:
			for _, v := range obj {
				ksqlValues = append(ksqlValues, v)
			}
		}
	}
	ksqlrow := map[string]interface{}{}
	for i, _ := range ksqlColNames {
		ksqlrow[ksqlColNames[i]] = ksqlValues[i]
	}
	return ksqlrow, nil
}
